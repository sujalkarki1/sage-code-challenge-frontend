jQuery(document).ready(function ($) {

    $(window).scroll(function () {
        doAnimateCss();
    });

    doAnimateCss();

    function doAnimateCss() {
        $('[data-animate-css]').each(function () {
            if ($(this).is(':in-viewport')) {
                animateCss($(this));
            }
        })
    }

    function animateCss(elements) {
        elements.each(function () {
            $(this).css('animation-delay', $(this).attr('data-animate-css-delay'));
            $(this).addClass('animated ' + $(this).attr('data-animate-css'));
            $(this).css('visibility', 'visible');
        })
    }

    //Script added for the mobile navigation
    $('#mobile-nav-trigger').click(function(){
        $(this).toggleClass('open');
        $(".primary-navigation").toggleClass('mobile-nav-show');
        $('body').toggleClass('mobile-nav-open');
    });

    var $window = $(window);

    function checkWidth() {
        //set main vars
        var windowsize = $window.width();

        if (windowsize <= 767) {
            $('.primary-navigation .sub-menu').hide();
            $('.primary-navigation .menu').children().click(function(){
                event.preventDefault();
                $(this).children('.primary-navigation .sub-menu').slideToggle('fast');
            });
        }
    }

    // Bind event listener and do initial execute
    $window.resize(checkWidth).trigger("resize");

    dragula($('.article-block .flex').toArray());
});


