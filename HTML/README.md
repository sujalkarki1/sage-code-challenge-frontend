﻿# README #

# Habitat HTML

This is a quick HTML example template that is developed using SASS.

**Tags:** Sass, Managed File, Typography


* Lets Start
* Version: 0.1

### How do I get set up? ###


* Install Node Package: npm install
* Run Gulp: gulp


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

## License ##
Copyright Sujal Karki